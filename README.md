# API

## Stack

- NodeJS : v17.5.0
- Express : v4.17.3

## Installation

```
npm install
```

### Running

```
NODE_ENV=local nodemon index.js
```
