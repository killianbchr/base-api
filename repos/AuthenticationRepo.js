'use strict';

const DB = require('../db/dbConnection');

exports.register = async function (params) {
  var query = `INSERT INTO users(username, email, password) VALUES ($1, $2, $3) RETURNING username;`;
  return new Promise((resolve, reject) => {
    return DB.connectDB().query(query, [params.username, params.email, params.password], (error, result) => {
      if (error && error.message.includes(`duplicate key value`)) {
        console.error(`Authentication : error register() =>  ${error}`);
        return reject('Conflict');
      }
      if (error && error.message.includes(`violates foreign key`)) {
        console.error(`Authentication : error register() =>  ${error}`);
        return reject('Bad request');
      }
      if (error) {
        console.error(`Authentication : error register() =>  ${error}`);
        if (error.message.includes(`duplicate key value`)) return reject(`Conflict`);
        return reject('Error server');
      }
      var response = result.rows[0];
      return resolve(response);
    });
  });
};
