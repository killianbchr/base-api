'use strict';

var Authentication = require('../services/AuthenticationService');

var utils = require('../utils/writer.js');
const { getError } = require('../utils/error.js');

module.exports.register = function (c, req, res) {
  const regexExp = /^[a-f0-9]{32}$/gi;

  if (!c.request.body.username || !c.request.body.email || !c.request.body.password) return getError(res, 'Bad request');
  if (!c.request.body.email.match(/^[\w-]+@[\w-]+\.[\w]+$/g)) return getError(res, 'Bad request');
  if (!regexExp.test(c.request.body.password)) return getError(res, 'Bad request');

  var params = {};
  params.username = c.request.body.username;
  params.email = c.request.body.email;
  params.password = c.request.body.password;

  Authentication.register(params)
    .then(function (response) {
      utils.writeJson(res, response, 201);
    })
    .catch(function (response) {
      getError(res, response);
    });
};
