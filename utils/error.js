var utils = require('../utils/writer.js');

/**
 *
 * @param {*} res
 * @param {*} response
 * @param {*} code
 */
module.exports.getError = function getError(res, error) {
  switch (error) {
    case 'bad request':
      return res.status(400).json({ err: 'bad request' });
    case 'Bad request':
      return res.status(400).json({ err: 'bad request' });
    case 'unauthorized':
      return res.status(401).json({ err: 'unauthorized' });
    case 'Unauthorized':
      return res.status(401).json({ err: 'unauthorized' });
    case 'forbidden':
      return res.status(403).json({ err: 'forbidden' });
    case 'Forbidden':
      return res.status(403).json({ err: 'forbidden' });
    case 'not found':
      return res.status(404).json({ err: 'not found' });
    case 'Not found':
      return res.status(404).json({ err: 'not found' });
    case 'conflict':
      return res.status(409).json({ err: 'conflict' });
    case 'Conflict':
      return res.status(409).json({ err: 'conflict' });
    case 'error server':
      return res.status(500).json({ err: 'error server' });
    case 'Error server':
      return res.status(500).json({ err: 'error server' });
    default:
      console.log(error);
      console.log('Unhandeld error.');
      return res.status(500).json({ err: 'error server' });
  }
};
