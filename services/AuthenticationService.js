'use strict';

const AuthenticationRepo = require('../repos/AuthenticationRepo');

exports.register = async function register(params) {
  return await AuthenticationRepo.register(params);
};
