'use strict';

const IdentifierMW = require('./middlewares/Identifier');

module.exports.router = {
  /**
   * POST /register
   * @param {*} c
   * @param {*} req
   * @param {*} res
   */
  register: async (c, req, res) => {
    const Authentication = require('./controllers/Authentication');
    return Authentication.register(c, req, res);
  },
};
